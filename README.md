# TP Final

Fecha: 07/12/2021

Nombre y apellido: Aulicino Matias

Materia: Infraestructura de Servidores

Profesor: Sergio Pernas

## Deployment AppCarga

Se describen los pasos para realizar el deployment del contenedor Apache Tomcat.

### Pre-requisitos

Sistema operativo con Docker instalado (Ubuntu).

Instalacion de Docker

```
$ sudo apt update
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
$ apt-cache policy docker-ce
$ sudo apt install docker-ce
```

### Instalación

Para crear la imagen del contendor debe descargar el archivo "Dockerfile" en su maquina con Docker instalado.

Crear imagen a partir del Dockerfile descargado.

```
# docker build -t "nombre_imagen" /path_Dockerfile
```

Una vez finalizada podemos ver la imagen creada.

```
# docker images
```

Ejecutamos el contenedor a partir de la imagen generada.

```
# docker run -d -p 8080:8080 --name "nombre_contenedor" "nombre_imagen"
```

Verificamos el estado del contenedor.

```
# docker ps -a
```



## Ejecutando las pruebas

Una vez que se realizo el deploy del contenedor podemos acceder a la app desde nuestro navegador

http://localhost:8080/appcarga


<a href="https://ibb.co/WVKLwBr"><img src="https://i.ibb.co/rdyVS5B/Captura.png" alt="Captura" border="0"></a><br />


## Ficheros 

Dockerfile

```
FROM ubuntu:latest
LABEL version="1.0"
LABEL description="Aplicacion web"
ARG DEBIAN_FRONTEND=noninteractive
#descarga fichero ectrypoint.sh desde Gitlab
ADD https://gitlab.com/matias.aulicino/appcarga/-/blob/main/entrypoint.sh / 
#permiso de ejecucion al fichero entrypoint.sh
RUN chmod +x /entrypoint.sh
#publico el puerto 8080
EXPOSE 8080
#declara entrypoint
ENTRYPOINT ["/entrypoint.sh"]
#Inicia tomcat y muestra logs
CMD sh /usr/local/tomcat/bin/catalina.sh start && tail -f /usr/local/tomcat/logs/catalina.out
```

entrypoint.sh

```
#!/bin/bash
set -e
#Instalacion de Apache Tomcat
apt update && apt install -y default-jdk apt-get -y install openjdk-8-jdk wget zip
#directorios para alojar la app
mkdir /usr/local/tomcat
cd /usr/local/tomcat/webapps && mkdir AppCarga
#descarga de la app desde la web
wget http://carloszuluaga.wdfiles.com/local--files/pruebascarga:instalar-app-ejemplo/EjemploPruebaCarga.zip -O /tmp/AppCarga.zip
#descomprimir app
cd /tmp && unzip AppCarga.zip -d /tmp
#copiar app al dircetrio de tomcat
cd /tmp/EjemploPruebaCarga/ && mv ./{,.[^.]}* /usr/local/tomcat/webapps/appcarga
cd /tmp && cp EjemploPruebaCarga.war /usr/local/tomcat/webapps
cd /usr/local/tomcat/webapps && mv EjemploPruebaCarga.war appcarga.war
exec "$@"
```



## Construido con

- [Docker](https://www.docker.com/)
- [Apache Tomcat](https://tomcat.apache.org/)

## Versionado

https://gitlab.com/matias.aulicino/appcarga/-/tree/main
Ver. 1.0.

## Autores

Aulicino Matias.
