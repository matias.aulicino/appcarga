FROM ubuntu:latest
LABEL version="1.0"
LABEL description="Aplicacion web"
ARG DEBIAN_FRONTEND=noninteractive
#descarga fichero ectrypoint.sh desde Gitlab
ADD https://gitlab.com/matias.aulicino/appcarga/-/blob/main/entrypoint.sh / 
#permiso de ejecucion al fichero entrypoint.sh
RUN chmod +x /entrypoint.sh
#publico el puerto 8080
EXPOSE 8080
#declara entrypoint
ENTRYPOINT ["/entrypoint.sh"]
#Inicia tomcat y muestra logs
CMD sh /usr/local/tomcat/bin/catalina.sh start && tail -f /usr/local/tomcat/logs/catalina.out
