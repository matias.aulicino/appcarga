#!/bin/bash
set -e
#Instalacion de Apache Tomcat
apt update && apt install -y default-jdk apt-get -y install openjdk-8-jdk wget zip
#directorios para alojar la app
mkdir /usr/local/tomcat
cd /usr/local/tomcat/webapps && mkdir AppCarga
#descarga de la app desde la web
wget http://carloszuluaga.wdfiles.com/local--files/pruebascarga:instalar-app-ejemplo/EjemploPruebaCarga.zip -O /tmp/AppCarga.zip
#descomprimir app
cd /tmp && unzip AppCarga.zip -d /tmp
#copiar app al dircetrio de tomcat
cd /tmp/EjemploPruebaCarga/ && mv ./{,.[^.]}* /usr/local/tomcat/webapps/appcarga
cd /tmp && cp EjemploPruebaCarga.war /usr/local/tomcat/webapps
cd /usr/local/tomcat/webapps && mv EjemploPruebaCarga.war appcarga.war
exec "$@"
